# gPlanner API Docs #

[![Documentation Status](https://readthedocs.org/projects/gplanner-api-docs/badge/?version=latest)](https://gplanner-api-docs.readthedocs.io/en/latest/?badge=latest)

This is root of gPlanner API documentation. You can read documentation on [readthedocs.io](https://gplanner-api-docs.readthedocs.io/en/latest/)