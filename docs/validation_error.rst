.. _ValidationError:

ValidationError
===============

Тип ValidationError
--------------

Ошибка валидации со списком полей.

**JSON Body:**

+---------+------------------------------------+----------+
| Field   | Description                        | Type     |
+=========+====================================+==========+
| fields  | список полей в которых была ошибка | string[] |
+---------+------------------------------------+----------+
| code    | код ошибки                         | string   |
+---------+------------------------------------+----------+
| message | описание ошибки                    | string   |
+---------+------------------------------------+----------+

**Codes:**

+---------------+-------------------------+
| Code          | Description             |
+===============+=========================+
| RequiredField | поле обязательно        |
+---------------+-------------------------+
| NotValidEnum  | неверное enum значение  |
+---------------+-------------------------+
| NotValidDate  | дата в неверном формате |
+---------------+-------------------------+