.. _statuses/edit:

PUT /statuses
==============

Edit review status
--------------

**Required role: MANAGER or ADMIN**

Редактирует текущей People status для определённого сотрудника.

-------------------

**REQUEST**

**JSON Parameters:**

+--------------+----------+---------------------+--------------------------+
| Parameter    | Required | Description         | Type                     |
+==============+==========+=====================+==========================+
| employee     | yes      | username сотрудника | string                   |
+--------------+----------+---------------------+--------------------------+
| loyality     | no       | лояльность          | :ref:`Loyality`          |
+--------------+----------+---------------------+--------------------------+
| reviewStatus | no       | ревью статус        | :ref:`ReviewStatus`      |
+--------------+----------+---------------------+--------------------------+
| comment      | no       | комментарий         | string                   |
+--------------+----------+---------------------+--------------------------+

---------------------

**RESPONSE**

**Successful**

====== ========================================
Status 200 Ok
Body   Информация об обновлённом статусе (тип :ref:`Status`)
====== ========================================


**Validation error**

====== ===============
Status 400 Bad Request
Body   array of :ref:`ValidationError`
====== ===============

**Status not found**

Сотруднику ещё не был присвоен status.

================= ===============
Status            400 Bad Request
Body              :ref:`ServiceError`
ClientError.code  StatusNotFound
================= ===============

**Employee not found**

================= ===============
Status            400 Bad Request
Body              :ref:`ServiceError`
ClientError.code  UserNotFound
================= ===============