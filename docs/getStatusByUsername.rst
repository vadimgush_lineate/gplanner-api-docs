.. _statuses/getStatus/username:

GET /statuses/{username}
==============

Get review status
-----------------------

Получает People status для заданного пользователя или пустой JSON объект если пользователю ещё не присваивали status.

---------------------

**RESPONSE**

**Successful**

====== ========================================
Status 200 Ok
Body   Review status (тип :ref:`Status`)
====== ========================================

**User not found**

====== ========================================
Status 404 Not Found
====== ========================================