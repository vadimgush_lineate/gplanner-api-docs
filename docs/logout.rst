.. _auth/logout:

GET /auth/logout
=====

Logout
--------------

Удаляет Cookie с токеном у пользователя.

---------------------

**RESPONSE**

**Always**

====== ======
Status 204 No content
====== ======

========== ==========
Название   Содержание
========== ==========
Set-Cookie token=; Max-Age=0; Expires=Thu, 01-Jan-1970 00:00:10 GMT
========== ==========