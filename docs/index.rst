gPlanner API Documentation
==========================

**gPlanner** - планнер для линейных менеджеров

Авторизация
-----------

Все методы требуют наличия JSON Web Token (далее именуемый как просто токен) для идентификации пользователя.

 * :ref:`auth/login` - авторизует пользователя и возвращает новый токен
 * :ref:`auth/logout` - удаляет токен из Cookie пользователя
 
Аккаунты
--------

Методы для управления аккаунтами:

 * :ref:`accounts/register` - регистрация аккаунтов
 * :ref:`accounts/getInfo` - поиск по сотрудникам
 * :ref:`accounts/getInfo/username` - получение информации об аккаунте пользователя
 * :ref:`accounts/update` - обновление информации об аккаунте
 * :ref:`accounts/delete` - удаление аккаунта

Статусы
-------------

Методы для управления статусами:

 * :ref:`statuses/getStatus` - получить статус текущего пользователя
 * :ref:`statuses/getStatus/username` - получить статус конкретного пользователя
 * :ref:`statuses/register` - создать новый статус
 * :ref:`statuses/edit` - редактировать статус

Методы для получения статистики:

 * :ref:`statuses/getStats/username` - получение статистики для конкретного пользователя
 * :ref:`statuses/getStats` - получение статистики для текущего пользователя

Типы объектов
-------------

:ref:`ValidationError` и :ref:`ServiceError` так же содержат себе список всех кодов ошибок.

 * :ref:`Account` - пользовательский аккаунт
 * :ref:`Status` - People status
 * :ref:`ValidationError` - список ошибок валидации
 * :ref:`ServiceError` - сервисная ошибка
 * :ref:`Stats` - статистика

Перечисления:

 * :ref:`Role` - роль пользователя
 * :ref:`Location` - локация сотрудника
 * :ref:`Direction` - направление
 * :ref:`ReviewStatus` - статус ревью
 * :ref:`Loyality` - лояльность