.. _Loyality:

Loyality
========

enum Loyality
--------------

Лояльность

**Values:**

+--------------+---------------+--------------------------+
| Value        | Numeric value | Description              |
+==============+===============+==========================+
| VERY_DIS     | 1             | Very dissatisfied        |
+--------------+---------------+--------------------------+
| SOMEWHAT_DIS | 2             | Somewhat dissatisfied    |
+--------------+---------------+--------------------------+
| NEITHER_SAT  | 3             | Neither satisfied or not |
+--------------+---------------+--------------------------+
| SOMEWHAT_SAT | 4             | Somewhat satisfied       |
+--------------+---------------+--------------------------+
| VERY_SAT     | 5             | Very satisfied           |
+--------------+---------------+--------------------------+