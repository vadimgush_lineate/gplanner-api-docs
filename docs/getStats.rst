.. _statuses/getStats:

GET /stats
==============

Get status statistics
-----------------------

Метод полностью аналогичный :ref:`statuses/getStats/username` за исключением того, что статистика возвращается для текущего пользователя.