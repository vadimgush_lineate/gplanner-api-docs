.. _auth/login:

POST /auth/login
=====

Login
--------------

Проводит авторизацию пользователя.

-------------------

**REQUEST**

**JSON parameters:**

========= ============ ===========
Parameter Required     Description
========= ============ ===========
username  yes          username
password  yes          password
========= ============ ===========

**Example:**

.. code-block:: JSON

  {
    "username": "vadim",
    "password": "temppass"
  }


---------------------

**RESPONSE**

**Successful**

====== ======
Status 204 No content
====== ======

========== ==========
Название   Содержание
========== ==========
Set-Cookie Сгенерированный JSON Web Token для данного пользователя
========== ==========

**Wrong credentials**

====== ===============
Status 401 Unauthorized
====== ===============

**Required fields are empty**

====== ===============
Status 400 Bad Request
Body   array of :ref:`ValidationError`
====== ===============