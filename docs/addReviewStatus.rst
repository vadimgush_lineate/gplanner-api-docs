.. _statuses/register:

POST /statuses
==============

Create new status
--------------

**Required role: MANAGER or ADMIN**

Создаёт новый People status и присваивает его определённому менеджеру и сотруднику

-------------------

**REQUEST**

**JSON Parameters:**

+--------------+----------+---------------------+--------------------------+
| Parameter    | Required | Description         | Type                     |
+==============+==========+=====================+==========================+
| employee     | yes      | username сотрудника | string                   |
+--------------+----------+---------------------+--------------------------+
| manager      | yes      | username менеджера  | string                   |
+--------------+----------+---------------------+--------------------------+
| loyality     | yes      | лояльность          | :ref:`Loyality`          |
+--------------+----------+---------------------+--------------------------+
| reviewStatus | yes      | ревью статус        | :ref:`ReviewStatus`      |
+--------------+----------+---------------------+--------------------------+
| comment      | no       | комментарий         | string                   |
+--------------+----------+---------------------+--------------------------+

---------------------

**RESPONSE**

**Successful**

====== ========================================
Status 200 Ok
Body   Информация о добавленном статусе (тип :ref:`Status`)
====== ========================================

**Validation error**

====== ===============
Status 400 Bad Request
Body   array of :ref:`ValidationError`
====== ===============

**Employee not found**

================= ===============
Status            400 Bad Request
Body              :ref:`ServiceError`
ClientError.code  UserNotFound
================= ===============

**Manager not found**

================= ===============
Status            400 Bad Request
Body              :ref:`ServiceError`
ClientError.code  ManagerNotFound
================= ===============