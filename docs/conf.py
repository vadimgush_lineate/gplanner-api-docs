project = u'gPlanner'
copyright = u'2019, Thumbtack'
author = u'Vadim Gush'

html_theme = "sphinx_rtd_theme"
html_theme_path = ["_themes", ]

master_doc = "index"