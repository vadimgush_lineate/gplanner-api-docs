.. _accounts/delete:

DELETE /accounts/{username}
==============

Deletes account
--------------

**Требуемая роль: ADMIN**

Помечает сотрудника как уволенного (поле fired).

---------------------

**RESPONSE**

**Successful**

====== ========================================
Status 204 No Content
====== ========================================


**User not found**

====== ===============
Status 404 Not Found
====== ===============